package ExerciseOfComposition.customer;

public class InvoiceTest {
    public static void main(String[] args) {
        Invoice i = new Invoice(99,99,"ujang",10*99,1000);
        System.out.println(i);
        System.out.println(i.getAmountAferDiscount());
        System.out.println(i.getCustomerName());
        i.setAmount(10000000);//Test set amount
        System.out.println(i.getAmountAferDiscount()); //Test get Amount afterdiscount

    }
}
