package ExerciseOfComposition.customer;

public class Invoice {
    private int ID;
    private Customer customer;
    private double amount;

    public Invoice(int ID, int id, String name,int discount, double amount){
        this.ID = id;
        customer = new Customer(id,name,discount);
        this.amount = amount;
    }
    public Invoice(int id, Customer customer, double amount) {
        this.ID = id;
        this.customer = customer;
        this.amount = amount;
    }

    public int getId() {
        return ID;
    }

    public Customer getCustomer() {
        return customer;
    }

    public double getAmount() {
        return amount;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCustomerName(){
        return customer.getName();
    }
    public double getAmountAferDiscount(){
        return amount - customer.getDiscount();
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "ID=" + ID +
                ", ExerciseOfComposition.customer=" + customer +
                ", amount=" + amount +
                '}';
    }
}
