package ExerciseOfComposition.my_point;

public class MyCircelPointTest {
    public static void main(String[] args){
        MyCircle c1 = new MyCircle(); //Test constructor
        System.out.println(c1);

        c1.setCenterXY(9,5);
        c1.setRadius(5);
        System.out.println(c1);// TestSetter

        MyPoint p1 = new MyPoint(5,10);
        MyCircle c2 = new MyCircle(p1,7);
        System.out.println("ExercisesOfClasses.Circle 2 : "+ c2);

        System.out.println(c1.getArea()); //Test getArea
        System.out.println(c1.getCircumference()); //Tes getCicum

        System.out.println(c1.distance(c2)); //Test Another ExercisesOfClasses.Circle

    }
}
