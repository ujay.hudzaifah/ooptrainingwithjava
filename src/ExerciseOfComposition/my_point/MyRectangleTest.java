package ExerciseOfComposition.my_point;

public class MyRectangleTest {
    public static void main(String[] args){
        MyRectangle r1 = new MyRectangle(7,8,5,9,6,5,9,2);
        System.out.println(r1); // test toString
        System.out.println(r1.getPerimeter()); // test perimeter
    }
}
