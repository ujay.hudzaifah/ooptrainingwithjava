package ExerciseOfComposition.my_point;

public class MyRectangle {
    private MyPoint t1;
    private MyPoint t2;
    private MyPoint t3;
    private MyPoint t4;

    public MyRectangle(int x1,int x2, int x3, int x4, int y1, int y2, int y3, int y4){
        t1 = new MyPoint(x1,y1);
        t2 = new MyPoint(x2,y2);
        t3 = new MyPoint(x3,y3);
        t4 = new MyPoint(x4,y4);
    }

    public MyRectangle(MyPoint t1, MyPoint t2, MyPoint t3, MyPoint t4) {
        this.t1 = t1;
        this.t2 = t2;
        this.t3 = t3;
        this.t4 = t4;
    }
     public double getPerimeter (){
        return t1.distance(t2) + t1.distance(t3) + t3.distance(t4) + t2.distance(t4);
     }

    @Override
    public String toString() {
        return "MyRectangle{" +
                "t1=" + t1 +
                ", t2=" + t2 +
                ", t3=" + t3 +
                ", t4=" + t4 +
                '}';
    }
}
