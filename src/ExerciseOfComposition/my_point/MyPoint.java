package ExerciseOfComposition.my_point;

public class MyPoint {
    private int x ;
    private int y ;

    public MyPoint(){
        x = 0;
        y = 0;
    }
    public MyPoint(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int[] getXY(){
        int getXY[] = new int[2];
        getXY[0] = x;
        getXY[1] = y;
        return getXY;
    }
    public void setXY(int x, int y){
        this.x = y;
        this.x = y;
    }

    public double distance(int x, int y){
        int aDiff = this.x - x;
        int bDiff = this.y - y;
        return Math.sqrt(aDiff*aDiff+bDiff*bDiff);
    }
    public double distance (MyPoint another){
        int aDiff = this.x - another.y;
        int bDiff = this.y - another.y;
        return Math.sqrt(aDiff*aDiff+bDiff*bDiff); //count distance a to b
    }
    public double distance() {
        return Math.sqrt(this.x*this.x + this.y*this.y); // distance from this point (0,0)
    }
    public String toString() {
        return "(" + this.x + ", " + this.y + ")";
    }

}
