package ExerciseOfComposition.my_point;

public class MyTriangleTest {
    public static void main(String[] args) {

        MyTriangle t1 = new MyTriangle(4, 5, 7, 8, 4, 7);
        System.out.println(t1); // test toString

        System.out.println(t1.getPerimeter());
        System.out.println(t1.getType()); // Test getType
    }

}
