package ExerciseOfComposition.my_point;


public class MyPointTest {
    public static void main(String[] args) {


    // Test program to test all constructors and public methods
        MyPoint p1 = new MyPoint(); //Test Constructor
        System.out.println(p1);
        p1.setX(8); //Test Setter
        p1.setY(6);
        System.out.println("a is : " + p1.getX()); //Test getter
        System.out.println("b is : " + p1.getY());

        System.out.println(p1.getXY()[0]);  // Test getXY()
        System.out.println(p1.getXY()[1]);
        System.out.println(p1);

        p1.setXY(3,0);
        System.out.println(p1.getXY()[0]); //Test setXY
        System.out.println(p1.getXY()[1]);

        MyPoint p2 = new MyPoint(0,4); // Test Another Constructor
        System.out.println(p2);
        System.out.println(p1.distance(p2));    // which version?
        System.out.println(p2.distance(p1));    // which version?
        System.out.println(p1.distance(5, 6));  // which version?
        System.out.println(p1.distance());      // which version?

        MyPoint[] pointsA = new MyPoint[11];
        MyPoint[] pointsB = new MyPoint[11];
        int pointValueX = 10;
        int pointValueY = 0;
        for (int a = 1; a<=10; a++){ // print looping array points

            pointsA[a] = new MyPoint(pointValueX-a,pointValueY+a);
            pointsB[a] = new MyPoint(pointValueY+a,pointValueX-a);

            System.out.println("NO " + a + " : " + pointsA[a].distance(pointsB[a])); //count distance points
        }

    }
}
