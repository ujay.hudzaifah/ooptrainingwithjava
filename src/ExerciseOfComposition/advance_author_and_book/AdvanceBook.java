package ExerciseOfComposition.advance_author_and_book;

import java.util.Arrays;

public class AdvanceBook {
    String name;
    AdvanceAuthor[] authors;
    double price;
    int qty;


    public AdvanceBook(String name, AdvanceAuthor[] author, double price, int qty) {
        this.name = name;
        this.authors = author;
        this.price = price;
        this.qty = qty;
    }

    public AdvanceBook(String name, AdvanceAuthor[] author, double price) {
        this.name = name;
        this.authors = author;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public AdvanceAuthor[] getAuthors() {
        return authors;
    }

    public double getPrice() {
        return price;
    }

    public int getQty() {
        return qty;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    @Override
    public String toString() {
        return "AdvanceBook{" +
                "name='" + name + '\'' +
                ", authors=" + Arrays.toString(authors) +
                ", price=" + price +
                ", qty=" + qty +
                '}';
    }

    public String getAuthorNames(){
        for (int a = 0; a<authors.length; a++){
            authors[a].getName();
        }
        return getAuthorNames();
    }
}
