package ExerciseOfComposition.advance_author_and_book;

public class AdvanceAuthorAndBookTest {
    public static void main (String[] args){
        AdvanceAuthor[] authors = new AdvanceAuthor [2];
        authors[0] = new AdvanceAuthor("Tan Ah Teck", "AhTeck@somewhere.com", 'm');
        authors[1] = new AdvanceAuthor("Paul Tan", "Paul@nowhere.com", 'm');

        AdvanceBook javaDummy = new AdvanceBook("javaDummy",authors,19.00,99);
        System.out.println(javaDummy);
    }
}
