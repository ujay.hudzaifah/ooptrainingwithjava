package ExerciseOfComposition.advance_author_and_book;


public class AdvanceAuthor {
        private String name;
        private String email;
        private char gender ;

        @Override
        public String toString() {
            return "Author{" +
                    "name='" + name + '\'' +
                    ", email='" + email + '\'' +
                    ", gender='" + gender + '\'' +
                    '}';
        }

        public AdvanceAuthor (String name, String email, char gender) {
            this.name = name;
            this.email = email;
            this.gender = gender;
        }

        public String getName() {
            return name;
        }

        public String getEmail() {
            return email;
        }

        public char getGender() {
            return gender;
        }
        public void setEmail(String email) {
            this.email = email;
        }

    }

