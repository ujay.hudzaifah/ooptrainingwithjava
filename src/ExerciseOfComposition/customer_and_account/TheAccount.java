package ExerciseOfComposition.customer_and_account;

public class TheAccount {
    private int id;
    private TheCustomer theCustomer;
    private double balance;

    public TheAccount(int id, TheCustomer theCustomer, double balance) {
        this.id = id;
        this.theCustomer = theCustomer;
        this.balance = balance;
    }

    public TheAccount(int id, TheCustomer theCustomer) {
        this.id = id;
        this.theCustomer = theCustomer;
        balance = 0.0;
    }

    public int getId() {
        return id;
    }

    public TheCustomer getTheCustomer() {
        return theCustomer;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
    public String getCustomerName(){
        return theCustomer.getName();
    }
    public TheAccount deposit (double amount){
        return deposit(balance+amount);

    }
    public TheAccount withDraw(double amount) {
        if (this.balance > amount){
            return withDraw(balance=balance+amount);
        }
        else {
            return withDraw(balance);
        }
    }

    @Override
    public String toString() {
        return "TheAccount{" +
                "id=" + id +
                ", theCustomer=" + theCustomer +
                ", balance=" + balance +
                '}';
    }
}
