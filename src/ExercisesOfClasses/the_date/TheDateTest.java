package ExercisesOfClasses.the_date;

public class TheDateTest {
    public static void main (String[] args){
        TheDate theDate = new TheDate(0,0,0);

        theDate.setDay(31);
        theDate.setMonth(10);
        theDate.setYear(2004);

        System.out.println(" "+ theDate.getDay() + "/" + " " + theDate.getMonth()+ "/" + " " + theDate.getYear());
    }

}
