package ExercisesOfClasses.invoice;

public class InvoiceItem {
    String id ;
    String desc;

    int qty;
    double uniprice;

    public InvoiceItem(String id, String desc, int qty, double uniprice) {
        this.id = id;
        this.desc = desc;
        this.qty = qty;
        this.uniprice = uniprice;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public double getUniprice() {
        return uniprice;
    }

    public void setUniprice(double uniprice) {
        this.uniprice = uniprice;
    }

    public double getTotal(){
        return uniprice*qty;
    }


    @Override
    public String toString() {
        return "InvoiceItem{" +
                "id='" + id + '\'' +
                ", desc='" + desc + '\'' +
                ", qty=" + qty +
                ", uniprice=" + uniprice +
                '}';
    }
}
