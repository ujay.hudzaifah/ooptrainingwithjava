import ExercisesOfClasses.invoice.InvoiceItem;

public class InvoiceTest{
    public static void main (String[] args){

        InvoiceItem invoiceItem = new InvoiceItem(null,null,0,0);

        invoiceItem.setId("239649127");
        invoiceItem.setDesc("Shoes");
        invoiceItem.setQty(8);
        invoiceItem.setUniprice(20000);

        System.out.println("ID : " + invoiceItem.getId() + " Quantity: " + invoiceItem.getQty() + " Description : " + invoiceItem.getDesc() + " Uniprice : "+ invoiceItem.getUniprice());
    }
}
