package ExercisesOfClasses.the_time;

public class TheTime {

    int  hour;
    int  minute;
    int  second;

    @Override
    public String toString() {
        return "TheTime{" +
                "hour=" + hour +
                ", minute=" + minute +
                ", second=" + second +
                '}';
    }


    public TheTime(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public void add(TheTime time) {
    }
}
