package ExercisesOfClasses.the_time;

import java.util.ArrayList;

public class TheTimeTest {

    public static void main(String args[])
    {
        int[] data = {07, 52, 25, 10, 19, 55, 15, 18, 41};
        int numberOfDates = data.length/3;
        ArrayList <TheTime> dates = new ArrayList< TheTime >(numberOfDates);

        for(int x=0; x < numberOfDates; x++){
            int index = x*3;
            TheTime time = new TheTime (data [index],data[index+1],data[index+2]);
            System.out.println("added date; "+time.toString());
            dates.add(time);

        }

    }
}
