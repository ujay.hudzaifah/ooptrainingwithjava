package ExercisesOfClasses.TheEmployeeClasses;

public class EmployeeTest {
    public static void main (String[] args){
        Employee employee = new Employee(002,"Muhammad"," Hudzaifah ",3000000);

        System.out.println("ID : " + employee.getId() + "Name :" + employee.getName()+ "Salary :" + employee.getSalary());
        System.out.println("Raise Salary " + employee.raiseSalary());

        Employee employee2 = new Employee(0,null,null,0);
        employee2.setId(12);
        employee2.setFirstName("Ujang");
        employee2.setLastName("Jajang");
        employee2.setSalary(5000);

        System.out.println("ID : " + employee2.getId() + "Name :" + employee2.getName()+ "Salary :" + employee2.getSalary());
        System.out.println("Raise Salary " + employee2.raiseSalary());
    }
}
