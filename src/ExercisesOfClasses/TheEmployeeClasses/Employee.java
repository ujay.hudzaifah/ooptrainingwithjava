package ExercisesOfClasses.TheEmployeeClasses;

public class Employee {
    public int id ;
    public String firstName;
    public String lastName;
    public double salary;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public Employee(int id, String firstName, String lastName, int salary) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
    }

    public String getName(){
        return id + " " + firstName + " " + lastName;
    }
    public double getAnnualSalary(){
        return salary*12;
    }
    public  double raiseSalary (){
        return (salary*30/100)+salary;
    }
    public String toString(){
       return  "Employee ID : " + id + "Name" + firstName + " " + lastName + "Salary" + salary;
    }
}
