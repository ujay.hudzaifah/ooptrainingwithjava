package ExercisesOfClasses.Circle;

public class CircleTest {
    public static void main (String[] args){
        // Declare an instance of ExercisesOfClasses.Circle.ExercisesOfClasses.Circle class called c1.
        // Construct the instance c1 by invoking the "default" constructor
        // which sets its radius and color to their default value.

        Circle c1 = new Circle(5.0);
        // Invoke public methods on instance c1, via dot operator.
        System.out.println("C1 = The circle has radius of "
                + c1.getRadius() + " and area of " + c1.getArea());

        System.out.println(c1.toString());

        // Declare an instance of class circle called c2.
        // Construct the instance c2 by invoking the second constructor
        // with the given radius and default color.

        Circle c2 = new Circle(5.0);
        // Invoke public methods on instance c2, via dot operator.
        System.out.println("C2 =The circle has radius of "
                + c2.getRadius() + " and area of " + c2.getArea());

        System.out.println(c2.toString());
        System.out.println(c2);
        System.out.println("Operator '+' invokes toString() too: " + c2);

        Circle c3 = new Circle(4.0);
        c3.setRadius(5.0); // construct an instance of circle
        System.out.println("radius is: " + c3.getRadius()); //print radius via getter
        c3.setColor("red ");//construct an instance of color
        System.out.println("color is: "+ c3.getColor());//print color via getter


//       /* ExercisesOfClasses.Circle.ExercisesOfClasses.Circle c4 = new ExercisesOfClasses.Circle.ExercisesOfClasses.Circle(5.0);
//        System.out.println(c4.toString());
//
//        ExercisesOfClasses.Circle.ExercisesOfClasses.Circle c5 = new ExercisesOfClasses.Circle.ExercisesOfClasses.Circle(1.2);
//        System.out.println(c5.toString());  // explicit call
//        System.out.println(c5);             // println() calls toString() implicitly, same as above
//        System.out.println("Operator '+' invokes toString() too: " + c5);  // '+' invokes toString() too*/


    }


}
