package ExercisesOfClasses.Circle;

public class Circle {
    // private instance variable, not accesable from outside this class
    private double radius;
    private String color;

    //The default constructor with no argument
    //Set radius and color default value

    public Circle(){
        radius = 1.0;
        color = "red";
    }
    // 2nd constructor with given radius, but color default
    public Circle (double radius){
        this.radius = radius;
    }
    public Circle (String color){
        this.color = color;
    }
    public void Circle (double radius, String color){
        this.radius = radius;
        this.color = color;
    }
    //Setter for intance variable radius
    public void setRadius (double radius){
        this.radius = radius;
    }
    //setter for instance variable color
    public void setColor (String color){
        this.color = color;
    }


    //Public method for retrieving the radius
    public double getRadius (){
        return radius;
    }

    //Public method for retrieving the color
    public String getColor (){
        return  color;
    }

    //Public method to computing the area of circle
    public double getArea (){
        return radius*radius*Math.PI;
    }

    public String toString(){
        return "ExercisesOfClasses.Circle [radius = "+ radius + " color = " + color +"]";
    }
}
