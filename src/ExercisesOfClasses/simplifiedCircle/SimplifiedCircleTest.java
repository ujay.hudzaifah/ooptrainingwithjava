package ExercisesOfClasses.simplifiedCircle;

public class SimplifiedCircleTest {
    public static void main (String[] args){

        SimplifiedCircle simplifiedCircle = new SimplifiedCircle(3.0);
        System.out.println("The ExercisesOfClasses.Circle has radius of : "+ simplifiedCircle.radius + " and area of : "+ simplifiedCircle.getArea());
        System.out.println(simplifiedCircle.toString());
    }
}
