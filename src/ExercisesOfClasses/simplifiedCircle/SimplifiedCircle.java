package ExercisesOfClasses.simplifiedCircle;

public class SimplifiedCircle {
    public double radius;


    public SimplifiedCircle (){
        radius = 1.0;
    }
    public SimplifiedCircle (double radius){
        this.radius =radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
    public double getRadius() {
        return radius;
    }
    @Override
    public String toString() {
        return "Cirlcle [ radius" + radius +
                ']';
    }

    public double getArea (){
        return radius*radius*Math.PI;
    }


}
