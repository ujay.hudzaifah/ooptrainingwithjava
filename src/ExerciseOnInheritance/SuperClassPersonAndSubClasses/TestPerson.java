package ExerciseOnInheritance.SuperClassPersonAndSubClasses;

public class TestPerson {
    public static void main (String[]args){
        Person p1 = new Person("Ujang", "Lembang");
        Student s1 = new Student("Ujang","Lembang","GDP",2,20000000);
        Staff sta1 = new Staff("Ujang","Lembang","BSI",2000000);
        System.out.println(sta1);
        System.out.println(p1);
    }
}
