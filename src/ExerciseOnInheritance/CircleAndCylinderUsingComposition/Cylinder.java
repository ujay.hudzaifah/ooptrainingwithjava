package ExerciseOnInheritance.CircleAndCylinderUsingComposition;

public class Cylinder {
    Circle base;
    double height;

    public  Cylinder(){
        base = new Circle();
        height = 1.0;
    }

    public Circle getBase() {
        return base;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }
}
