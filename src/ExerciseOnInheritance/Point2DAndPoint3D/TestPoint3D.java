package ExerciseOnInheritance.Point2DAndPoint3D;

public class TestPoint3D {
    public static void main (String[]args){
        Point3D point = new Point3D(3,6,5);
        System.out.println(point);
        Point2D point2 = new Point2D(4,6);
        System.out.println(point2);
    }
}
