package ExerciseOnInheritance.Point2DAndPoint3D;

public class Point3D extends Point2D {
    private float z;

    public Point3D(float x, float y, float z) {
        super(x, y);
        this.z = z;
    }

    public Point3D() {
        this.z = 0.0f;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public void setXYZ(float x, float y, float z){
        super.setXY(x,y);
        this.z = z;
    }
    public float[] getXYZ (){
        float[] getXYZ = new float[3];
       getXYZ()[0] = getX();
       getXYZ()[1] = getY();
       getXYZ()[2] = z;
       return getXYZ();
    }

    @Override
    public String toString() {
        return super.toString()+", z=" + z;
    }
}
