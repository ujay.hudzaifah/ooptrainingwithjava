package ExerciseOnInheritance.PointAndMovablePoint;

public class TestMovablePoint {
    public static void main (String[]args){
        Point p1 = new Point(3,5);
        System.out.println(p1);
        MovablePoint move1 = new MovablePoint(2,6);
        move1.setX(4);
        move1.setY(5);
        System.out.println(move1);
        System.out.println(move1.getxSpeed());
        System.out.println(move1.getySpeed());
        move1.setSpeed(6,9);
        System.out.println(move1);
        move1.setxSpeed(22);
        move1.setySpeed(7);
        System.out.println(move1);
    }
}
