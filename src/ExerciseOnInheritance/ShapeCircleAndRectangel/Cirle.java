package ExerciseOnInheritance.ShapeCircleAndRectangel;

public class Cirle extends Shape {
    private double radius;

    public Cirle(String color, boolean filled, double radius) {
        super(color = "green",true);
        this.radius = radius;
    }

    public Cirle(double radius) {
        this.radius = radius;
    }
    public Cirle () {
        radius = 1.0;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
    public double getArea(){
        return radius*radius*Math.PI;
    }
    public double getPerimeter(){
        return 2*radius*Math.PI;
    }
    @Override
    public String toString() {
        return " A Shape With Radius  " + radius + " With a subclass of " + super.toString();
    }
}
