package ExerciseOnInheritance.ShapeCircleAndRectangel;

public class Square extends Rectangle {
    public Square(){
        super();
    }
    public Square(double side){
        super(side, side); // Call superclass Rectangle(double, double)
    }
    public Square(double side, String color, boolean filled){
        super(color,filled,side,side);
    }
    public double getSide(){
        return super.getWidht();
    }
    public void setSide(double side){
        setWidht(side);
        setLenght(side);
    }

    @Override
    public void setWidht(double side) {
        super.setWidht(side);
        super.setLenght(side);
    }

    @Override
    public void setLenght(double side) {
        super.setLenght(side);
        super.setWidht(side);
    }

    @Override
    public double getArea() {
        return super.getArea();
    }

    @Override
    public String toString() {
        return "A Square with side "+ getSide() + " Wich is a subclass of : " + super.toString();
    }
}
