package ExerciseOnInheritance.ShapeCircleAndRectangel;

public class Rectangle extends Shape {
    private double widht;
    private double lenght;

    public Rectangle(String color, boolean filled, double widht, double lenght) {
        super(color, filled);
        this.widht = widht;
        this.lenght = lenght;
    }

    public Rectangle(double widht, double lenght) {
        this.widht = widht;
        this.lenght = lenght;
    }

    public Rectangle (){
        widht = 1.0;
        lenght = 1.0;
    }

    public double getWidht() {
        return widht;
    }

    public void setWidht(double widht) {
        this.widht = widht;
    }

    public double getLenght() {
        return lenght;
    }

    public void setLenght(double lenght) {
        this.lenght = lenght;
    }
    public double getArea(){
        return widht*lenght;
    }
    public double gePerimeter(){
        return 2*widht*lenght;
    }

    @Override
    public String toString() {
           return  " A Rectangle with " + widht + " and " + lenght + " subclass of : " + super.toString();
    }
}
