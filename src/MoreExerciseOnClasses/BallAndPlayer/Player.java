package MoreExerciseOnClasses.BallAndPlayer;

public class Player {
    private int number;
    private float x;
    private float y;
    private float z;

    public Player(int number, float x, float y, float z) {
        this.number = number;
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public void move ( float xDisp, float yDisp){
        x+=xDisp;
        y+=yDisp;
    }
    public void jump (float zDsip){
        z+=zDsip;
    }

    public boolean near (Ball ball){
        if (ball.getX()<8){
            return true;
        }
        if (ball.getY()<8){
            return true;
        }
        else
            return false;
    }


}
