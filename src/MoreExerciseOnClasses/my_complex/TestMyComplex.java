package MoreExerciseOnClasses.my_complex;

public class TestMyComplex {
    public static void main(String[] args){
        MyComplex myComplex = new MyComplex(1,3);
        System.out.println(myComplex); //Test toString
        System.out.println(myComplex.isReal());
        MyComplex myComplexB = new MyComplex(5,9);
        System.out.println(myComplex.equals(myComplexB));
        System.out.println(myComplex.add(myComplexB));
        System.out.println(myComplex.magnitude());
        System.out.println(myComplex.multiply(myComplexB));
    }
}
