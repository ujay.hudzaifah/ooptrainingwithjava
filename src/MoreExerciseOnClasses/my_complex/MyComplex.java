package MoreExerciseOnClasses.my_complex;

public class MyComplex {
    double real;
    double imag;

    public MyComplex(double real, double imag){
        this.real = real;
        this.imag = imag;
    }
    public  MyComplex(){
        real = 0.0;
        imag = 0.0;
    }

    public double getReal() {
        return real;
    }

    public double getImag() {
        return imag;
    }

    public void setReal(double real) {
        this.real = real;
    }

    public void setImag(double imag) {
        this.imag = imag;
    }
    public void setValue (double real, double imag){
        this.real = real;
        this.imag = imag;
    }

    @Override
    public String toString() {
        return "("+ real + " + " + imag + "i" + ")";
    }

    public boolean isReal(){
        return imag == 0 ;
    }

    public boolean isImag(){
        return real ==0;
    }

    public boolean equals (double real, double imag){
        return (this.real == real && this.imag == imag);
    }
    public boolean equals (MyComplex another){
        return (this.real == another.real && this.imag == another.imag);
    }

    public double magnitude(){
        return Math.sqrt(real*real + imag*imag);
    }
    public  double argument (){
        return Math.atan2(imag, real);
    }
    public MyComplex add (MyComplex right){
        return new MyComplex(real + right.getReal(),imag + right.getImag());
    }
    public MyComplex addNew (MyComplex right){
        return new MyComplex(real,imag);
    }
    public MyComplex subtact (MyComplex right){
        return new MyComplex( (real - right.getReal()), imag - right.getImag());
    }
    public MyComplex multiply (MyComplex right){
        double real = this.real * right.getReal() - this.imag * right.getImag();
        double imag = this.real * right.getImag() + this.imag * right.getReal();
        return new MyComplex(real,imag);

    }
    public MyComplex divide (MyComplex right){
        double a = (real +  imag)*(right.real - right.imag);
        double b = (right.real * right.real) + (right.imag * right.imag);
        return new MyComplex(a,b);
    }

    public MyComplex conjugate(){
        return new MyComplex(real,-imag);
    }

}
