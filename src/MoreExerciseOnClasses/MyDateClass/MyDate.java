package MoreExerciseOnClasses.MyDateClass;



public class MyDate {
    private static int MIN_Year = 1;
    private static int MAX_YEAR = 9999;

    private int year;
    private int month;
    private int day;

    private static String[] strMonths = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
            "Aug", "Sep", "Oct", "Nov", "Dec"};
    private static String[] strDays = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
    private static int[] daysInMonths = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public static boolean isLeapYear(int year) {
        return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);
    }

    public static boolean isValidDate(int year, int month, int day) {
        if (year < MIN_Year || year > MAX_YEAR) {
            return false;
        }
        if (month < 1 || month > 12) {
            return false;
        }
        int maxDays = daysInMonths[month - 1] + ((isLeapYear(year) && month == 2) ? 1 : 0);
        return (day >= 1 && day <= maxDays);
    }

    public static int getDayOfWeek(int year, int month, int day) {
        if (!isValidDate(year, month, day)) {
            return -1;
        }
        int magicCenturyNumber = 6 - 2 * ((year / 100) % 4);
        int lastTwoDigitsOfYears = year % 100;
        int magicYearNumber = lastTwoDigitsOfYears % 4;

        int[] nonLeapYearMonthNumers = {0, 3, 3, 6, 1, 4, 6, 2, 5, 0, 3, 5};
        int[] leapYeasMonthNumbers = {6, 2, 3, 6, 1, 4, 6, 2, 5, 0, 3, 5};
        int magicMonthNumber = (isLeapYear(year) ? leapYeasMonthNumbers[month - 1] : nonLeapYearMonthNumers[month - 1]);

        int magicDayNumber = day;

        return (magicCenturyNumber + lastTwoDigitsOfYears + magicCenturyNumber + magicYearNumber
                + magicMonthNumber + magicDayNumber) % 7;

    }
    public MyDate (int year, int month, int day){
        try{
            setDate(year, month, day);
        }catch (IllegalArgumentException iae){
            System.out.println(iae.getMessage());
        }

    }

    public void setDate(int year, int moth, int day) throws IllegalArgumentException {
        if (isValidDate(year, month, day)){
            this.year = year;
            this.month = moth;
            this.day = day;
        }
        else {
            throw new IllegalArgumentException("Invalid year[1-9999], month[1-12], or day[1-28|29|30|31]!");
        }
    }
    public void setYear(int year) throws IllegalArgumentException {
        if (year >= MIN_Year && year <= MAX_YEAR) {
            this.year = year;
        } else {
            throw new IllegalArgumentException("invalid year ! [1-9999]");
        }
    }
    public void setMonth(int month) throws IllegalArgumentException {
        if (month >= 1 && month <= 12) {
            this.month = month;
        } else {
            throw new IllegalArgumentException("Invalid month![1-12]");
        }
    }
    public void setDay(int day) throws IllegalArgumentException {
        if ((day >= 1 && day <= daysInMonths[month - 1])
                || (isLeapYear(year) && month == 2 && day >= 1 && day <= 29)) {
            this.day = day;
        } else {
            throw new IllegalArgumentException("Invalid day![1-28|29|30|31]");
        }
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }



    @Override
    public String toString() {
        if (!isValidDate(year, month, day)) {
            return "Not a valid date!";
        }
        return strDays[getDayOfWeek(year, month, day)] + " " + day + " "
                + strMonths[month - 1] + " " + year;
    }

    public MyDate nextDay() throws IllegalArgumentException {
        day++;
        int maxDays = daysInMonths[month - 1] + ((isLeapYear(year) && month == 2) ? 1 : 0);
        if (day > maxDays) {
            day = 1;
            month++;
        }
        if (month > 12) {
            month = 1;
            year++;
        }
        if (year > MAX_YEAR) {
            throw new IllegalArgumentException("Yesr out of range! [1-9999]");
        }
        return this;
    }

    public MyDate nextMoth() {
        month++;
        if (month > 12) {
            month = 1;
            year++;
        }
        if (year > MAX_YEAR) {
            throw new IllegalArgumentException("Year out of range1 [1 - 9999]");
        }
        return this;
    }
    public MyDate nextYear() throws IllegalArgumentException {
        year++;
        if ( year > MAX_YEAR){
            throw new IllegalArgumentException("Year out of range! [1-9999]");
        }
        int maxDays = daysInMonths[month-1]+ ((isLeapYear(year)&& month == 2) ? 1 : 0);
        if (day > maxDays) {
            day = maxDays;
        }
        return this;
    }
    public MyDate perviousDay (){
        day -- ;
        if ( day < 1){
            day = 31;
            month --;
        }
        if (month < 1){
            month = 12;
            year --;
        }
        if ( year < MAX_YEAR){
            throw new IllegalArgumentException ("Year out of range [1-9999]") ;
        }
        return this;
    }
    public MyDate perviousMonth (){
        month--;
        if (month < 1){
            month = 12;
            year --;

        }
        if (year < MAX_YEAR){
            throw new IllegalArgumentException("Year out of rang [1-9999]");
        }
        return this;
    }
    public MyDate perviousYear(){
        year --;
        if (year < 1 ){
            throw new IllegalArgumentException("Year out of range [1-9999]");
        }
        int maxDays = daysInMonths[-1]+((isLeapYear(year)&& month == 2)? 1 : 0);
        if (day >maxDays ){
             day = maxDays;
        }
        return  this;   
    }
}