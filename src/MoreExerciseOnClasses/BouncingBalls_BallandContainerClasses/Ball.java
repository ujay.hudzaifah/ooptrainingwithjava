package MoreExerciseOnClasses.BouncingBalls_BallandContainerClasses;

public class Ball {
    private float x;
    private float y;
    private int radius;
    private float xDelta;
    private float yDelta;

    public Ball (float x, float y, int radius, int speed, int direction){
        double directionInRadians = Math.toRadians(direction);
        this.x = x;
        this.y = y;
        xDelta = speed * (float) Math.cos(directionInRadians);
        yDelta = -speed * (float) Math.sin(directionInRadians);

    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public float getxDelta() {
        return xDelta;
    }

    public void setxDelta(float xDelta) {
        this.xDelta = xDelta;
    }

    public float getyDelta() {
        return yDelta;
    }

    public void setyDelta(float yDelta) {
        this.yDelta = yDelta;
    }

    public void setXY (){
        this.x = x;
        this.y = y;
    }
    public void move (){
        x+= xDelta;
        y += yDelta;
    }
    public void reflectHorizontal(){
        xDelta = -xDelta;
    }
    public void reflectVertical(){
        yDelta = - yDelta;
    }
    public String toString() {
        return "Ball at (" + x + ", " + y + ") of velocity (" + xDelta + ", " + yDelta + ")";
    }
}
