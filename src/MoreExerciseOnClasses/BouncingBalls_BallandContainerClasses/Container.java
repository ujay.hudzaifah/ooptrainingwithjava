package MoreExerciseOnClasses.BouncingBalls_BallandContainerClasses;

public class Container {
    private int x1;
    private int x2;
    private int y1;
    private int y2;

    public Container (int x, int y, int widht, int height){
            x1 = x;
            y1 = y;
            x2 = x1+widht-1;
            y2 = y1 + widht-1;
    }

    public int getX1() {
        return x1;
    }

    public int getX2() {
        return x2;
    }

    public int getY1() {
        return y1;
    }

    public int getY2() {
        return y2;
    }

    public boolean collides (Ball ball){
        if (ball.getX() - ball.getRadius() <= this.x1||
            ball.getX() - ball.getRadius() >= this.x2){
            ball.reflectHorizontal();
            return true;
        }
        if (ball.getY() - ball. getRadius() <= this.y1||
            ball.getY() - ball.getRadius() >= this.y2){
            ball.reflectVertical();
            return true;
        }
        return false;
    }
    public String toString (){
        return "Container [(x1" + getX1() +"," + "y1" + getY1() + "),(" + "x2" + getX2() + "y2" + getY2() + ")";
    }


}
