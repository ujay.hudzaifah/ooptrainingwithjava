package ExerciseOnPolymorphism.InterfaceGeometriObjectAndResizable;

public class TestResizableCircle {
    public static void main(String[] args) {
        GeometriObject geomObj1 = new Circle(5.0);
        System.out.println(geomObj1);
        System.out.println("Perimeter = " + geomObj1.getPerimetri());
        System.out.println("Area = " + geomObj1.getArea());

        Resizable geomObj2 = new ResizableCircle(5.0);
        System.out.println(geomObj2);

        geomObj2.resize(50);
        System.out.println(geomObj2);

        GeometriObject geomObj3 = (GeometriObject) geomObj2;
        System.out.println(geomObj3);
        System.out.println("Perimeter = " + geomObj3.getPerimetri());
        System.out.println("Area = " + geomObj3.getArea());
    }
}
