package ExerciseOnPolymorphism.InterfaceGeometriObjectAndResizable;

public interface Resizable {
    public void resize (int percent);
}
