package ExerciseOnPolymorphism.InterfaceGeometriObjectAndResizable;

public interface GeometriObject {
    public double getPerimetri();
    public double getArea();


}
