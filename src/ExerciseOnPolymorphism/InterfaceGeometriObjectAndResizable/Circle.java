package ExerciseOnPolymorphism.InterfaceGeometriObjectAndResizable;

public class Circle implements GeometriObject{
    protected double radius;

    public Circle (){
        radius = 1.0;
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double getPerimetri() {
        return radius*radius*Math.PI;
    }

    @Override
    public double getArea() {
        return 2*radius*Math.PI;
    }

    @Override
    public String toString() {
        return "ExercisesOfClasses.Circle{" +
                "radius=" + radius +
                '}';
    }
}
