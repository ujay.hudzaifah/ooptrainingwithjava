package ExerciseOnPolymorphism.Polymorphism;

public class BigDog extends Dog {
    @Override
    public void greeting() {
        System.out.println("Wooow");
    }
    public void greeting(BigDog another){
        System.out.println("Wooooooooooooow");
    }
}
