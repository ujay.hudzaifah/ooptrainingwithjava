package ExerciseOnPolymorphism.InterfaceMovableAndItsImplementationsMovablePointAndMovableCircle1;

public class MovableCircle implements Movable {

    private MovablePoint center;
    private int radius;

    public MovableCircle (int x, int y, int xSpeed, int ySpeed, int radius){
        center = new MovablePoint(x, y, xSpeed, ySpeed);
        this.radius = radius;
    }

    public MovableCircle(MovablePoint center, int radius) {
        this.center = center;
        this.radius = radius;
    }
    @Override
    public String toString() {
        return "MovableCircle with radius=" + radius + " and center at " + center;
    }

    @Override
    public void moveUp() {
        center.y -= center.ySpeed;
    }

    @Override
    public void moveDown() {
        center.y += center.ySpeed;
    }

    @Override
    public void moveLeft() {
        center.x -= center.xSpeed;
    }

    @Override
    public void moveRight() {
        center.y += center.xSpeed;
    }
}
