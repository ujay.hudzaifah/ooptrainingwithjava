package ExerciseOnPolymorphism.AbstractSuperClassShapeAndItsConcreteSubClasses;

public class Square extends Rectangle {
    public Square (){
        super();
    }
    public Square(double side){
        super(side, side);
    }
    public Square(double side, String color, boolean filled){
        super(color, filled, side, side);
    }
    public double getSide(){
        return getWidht();
    }
    public void setSide(double side){
        setWidht(side);
        setLenght(side);
    }

    @Override
    public void setWidht(double side) {
        super.setWidht(side);
        super.setLenght(side);
    }

    @Override
    public void setLenght(double side) {
        super.setLenght(side);
        super.setLenght(side);
    }
}
