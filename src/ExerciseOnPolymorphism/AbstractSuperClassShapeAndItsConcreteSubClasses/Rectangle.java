package ExerciseOnPolymorphism.AbstractSuperClassShapeAndItsConcreteSubClasses;

public class Rectangle extends Shape {
    protected double widht;
    protected double lenght;

    public Rectangle(String color, boolean filled, double widht, double lenght) {
        super(color, filled);
        this.widht = widht;
        this.lenght = lenght;
    }

    public Rectangle(double widht, double lenght) {
        this.widht = widht;
        this.lenght = lenght;
    }
    public Rectangle(){
        widht = 0.0;
        lenght = 0.0;
    }

    public double getWidht() {
        return widht;
    }

    public void setWidht(double widht) {
        this.widht = widht;
    }

    public double getLenght() {
        return lenght;
    }

    public void setLenght(double lenght) {
        this.lenght = lenght;
    }

    public double getArea() {
        return widht * lenght;
    }

    public double getPerimeter() {
        return 2 *( widht + lenght);
    }

    @Override
    public String toString() {
        return String.format("A Rectangle with width = %f and length = %f, which is a subclass of %s"
                , widht, lenght, super.toString());
    }
}
