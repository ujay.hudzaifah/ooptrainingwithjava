package MoreExerciseOOP.polylineOfPointWithArrayList;

import java.util.ArrayList;
import java.util.List;

public class Polyline {
    private List<Point> points;

    public Polyline (){
        points = new ArrayList<Point>();
    }
    public Polyline(List points){
        this.points = points;
    }

    public void appendPoint (int x, int y){
        Point newPoint = new Point(x,y);
        points.add(newPoint);
    }
    public void appendPoint(Point point){
        points.add(point);
    }

    public String toString() {
        // Use a StringBuilder to efficiently build the return String
        StringBuilder sb = new StringBuilder("{");
        for (Point aPoint : points) {
            sb.append(aPoint.toString());
        }
        sb.append("}");
        return sb.toString();
    }
}
