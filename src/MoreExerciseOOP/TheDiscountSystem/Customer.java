package MoreExerciseOOP.TheDiscountSystem;

public class Customer {
    private String name;
    private boolean member;
    private String memberType;

    public Customer (String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean isMember() {
        return member;
    }

    public String getMemberType() {
        return memberType;
    }

    public void setMember(boolean member) {
        this.member = member;
        memberType = "Silver";
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    public String toString() {
        return "Customer "
                + name
                + (member ? " is a " : " is not a ")
                + "member of discount system."
                + ((member && memberType != null) ? " Member type is "
                + memberType : "");
    }
}
