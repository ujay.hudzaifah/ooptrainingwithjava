package MoreExerciseOOP.TheDiscountSystem;

import java.util.Date;

public class Visit {
    private Customer customer;
    private Date date;
    private double serviceExpense;
    private double productExpense;

    public Visit (String name, Date date){
        this.date = date;
        customer = new Customer(name);
    }


    public double getServiceExpense() {
        return serviceExpense;
    }

    public void setProductExpense(double productExpense) {
        this.productExpense = productExpense - productExpense
        * DiscountRate.getProductDiscountRate(customer.getMemberType());
    }

    public double getProductExpense() {
        return productExpense;
    }

    public void setServiceExpense(double serviceExpense){
        this.serviceExpense = serviceExpense - serviceExpense
                * DiscountRate.getProductDiscountRate(customer.getMemberType());
    }

    public double getTotalExpense() {
        return serviceExpense + productExpense;
    }

    @Override
    public String toString() {
        return date + "\n" + customer + "\nProduct expense: " + productExpense
                + "\nService expense: " + serviceExpense + "\nTotal expense: "
                + getTotalExpense();
    }
}
