package MoreExerciseOOP.TheDiscountSystem;

public class DiscountRate {
    static double serviceDiscountPremium;
    static double serviceDiscountGold;
    static double serviceDiscountSilver;
    static double productDiscountPremium;
    static double productDiscountGold;
    static double producctDiscountSilver;

    public DiscountRate(){
        serviceDiscountPremium = 0.2;
        serviceDiscountGold = 0.15;
        serviceDiscountSilver = 0.1;
        productDiscountPremium = 0.1;
        productDiscountGold = 0.1;
        producctDiscountSilver = 0.1;
    }

    public static double getServiceRate (String type){
        if (type == "Premium"){
            return serviceDiscountPremium;
        }
        else if (type == "Gold"){
            return serviceDiscountGold;
        }
        else if (type == "Silver"){
            return serviceDiscountSilver;
        }
        else {
            return 0.0;
        }
    }

    public static double getProductDiscountRate (String type) {
        if (type == "Premium") {
            return productDiscountPremium;
        }
        if (type == "Gold") {
            return productDiscountGold;
        }
        if (type == "Silver") {
            return producctDiscountSilver;
        }
        else {
            return 0.0;
        }
    }
}
